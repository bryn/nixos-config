local wezterm = require 'wezterm';

return {
  color_scheme = "Base16";
  color_schemes = {
    ["Ayu Dark"] = {
      foreground = "#e6e1cf",
      background = "black",
      cursor_fg = "black",
      cursor_bg = "#f29718",
      cursor_border = "#f29718",
      ansi =  {"#01060e", "#ea6c73", "#91b362", "#f9af4f", "#53bdfa", "#fae994", "#90e1c6", "#c7c7c7"},
      brights = {"#686868", "#f07178", "#c2d94c", "#ffb454", "#59c2ff", "#ffee99", "#95e6cb", "#ffffff"},
    },
    ["Base16"] = {
      foreground = "#e6e1cf",
      background = "black",
      cursor_fg = "black",
      cursor_bg = "#f29718",
      cursor_border = "#f29718",
      ansi = {"#000000", "#cb0119", "#86ab3b", "#ed8702", "#3c95be", "#c251ab", "#48b19c", "#d8d8d8"},
      brights = { "#b0b090" , "#fb0120" , "#a1c659" , "#fda331" , "#6fb3d2" , "#d381c3" , "#76c7b7" , "#ffffff"},
    }
  };
  enable_tab_bar = false;
  enable_wayland = true;
  exit_behavior = "Close";
  font = wezterm.font("Iosevka");
  font_size = 13.5;
}
